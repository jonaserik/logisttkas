<?php

require_once 'database.php';

class Rota {
	public $origem;
	public $destino;
	public $autonomia;
	public $valor_litro;
	public $malha;

	public function __construct()
	{
		/*
		 * TODO: Carregar o mapa aqui
		 */
		$this->malha = array(
			'A B 10',
			'B D 15',
			'A C 20',
			'C D 30',
			'B E 50',
			'D E 30'
		);
	}

	public function obterRota()
	{
		$dados = array(
			'orig' => $_POST['origem'],
			'dest' => $_POST['destino'],
			'auto' => $_POST['autonomia'],
			'fuel' => $_POST['preco_combustivel'],
		);

		return $this->calculoRota($dados);
	}

	public function malhaToDB($malha)
	{
		$malha_formato_db = '';
		foreach ($malha as $linha) {
			$malha_formato_db .= $linha.';';
		}

		return $malha_formato_db;
	}

	public function atualizarMapa($usuario, $nome_mapa, $malha)
	{
		$res = Query::setNovoMapa($usuario, $nome_mapa, $this->malhaToDB($malha));

		if ($res) {
			return true;
		}
		return false;
	}

	public function inputRules(array $input)
	{
		$erros = array();

		foreach ($input as $item) {
			if (strlen($item['valor']) < 1) {
				$erros[] = "O campo ".$item['campo']." precisa ser preenchido";
			}

			switch($item['tipo']) {
				case 'float' :
					if (! is_numeric($item['valor'])) {
						$erros[] = "O campo ".$item['campo']." precisa ser do tipo ".$item['tipo'];
					}
			}
		}

		if ( ! empty($erros)) {
			array_unshift($erros, sizeof($erros)." ERRO(S) encontrado(s)");
		}

		return $erros;
	}

	public function calculoRota($dados)
	{
		$campos = array(
			array (
				'campo'     => 'Origem',
				'valor'     => $dados['orig'],
				'tipo'      => 'string',
			),
			array (
				'campo'     => 'Destino',
				'valor'     => $dados['dest'],
				'tipo'      => 'string',
			),
			array (
				'campo'     => 'Autonomia',
				'valor'     => $dados['auto'],
				'tipo'      => 'float',
			),
			array (
				'campo'     => 'Preco Combustivel',
				'valor'     => $dados['fuel'],
				'tipo'      => 'float',
			)
		);

		$validacao = $this->inputRules($campos);

		if (empty($validacao)) {
			$this->origem       = $dados['orig'];
			$this->destino      = $dados['dest'];
			$this->autonomia    = $dados['auto'];
			$this->valor_litro  = $dados['fuel'];
		} else {
			return json_encode($validacao);
		}

		if ($dados['orig'] == $dados['dest']) {
			$rota['rota']   = "Origem e destino iguais";
			$rota['custo']  = 0;
			return json_encode($rota);
		}

		$caminho_direto = $this->procurarCaminhoDireto($dados['orig'], $dados['dest']);

		if ($caminho_direto) {
			$custo = ($caminho_direto[2] / $dados['auto']) * $dados['fuel'];

			$rota['rota']   = $caminho_direto[0] . ' ' . $caminho_direto[1];
			$rota['custo']  = $custo;

			return json_encode($rota);

		} else {
			$caminho_indireto = $this->procurarCaminhoIndireto($dados['orig'], $dados['dest']);

			$custo = ($caminho_indireto['km'] / $dados['auto']) * $dados['fuel'];
			$rota['rota'] = '';

			foreach ($caminho_indireto as $index) {
				if (!is_numeric($index)) {
					$rota['rota'] .= $index ." ";
				}
			}
			$rota['rota'] = trim($rota['rota']);
			$rota['custo']  = $custo;
			return json_encode($rota);
		}
	}

	public function procurarCaminhoDireto($orig, $dest)
	{
		foreach ($this->malha as $key => $value) {
			$linha = explode(' ', $value);
			$o = array_search($orig, $linha);
			$d = array_search($dest, $linha);
			if (is_numeric($o) && is_numeric($d)) {
				return $this->conferirDirecao($orig, $dest, $linha);
			}
		}
		return false;
	}

	public function conferirDirecao($orig, $dest, $linha)
	{
		if ($orig == $linha[1]) {
			$linha[0] = $orig;
			$linha[1] = $dest;
		}
		return $linha;
	}

	public function procurarCaminhoIndireto($orig, $dest)
	{
		$caminhos_possiveis = array();

		foreach ($this->malha as $key => $value) {
			$linha = explode(' ', $value);
			$o = array_search($orig, $linha);
			$d = array_search($dest, $linha);

			if (is_numeric($o)) {
				$caminhos_possiveis[] = $value;
			}

			if (is_numeric($d)) {
				$caminhos_possiveis[] = $value;
			}
		}

		return $this->tracarRotaComposta($orig, $dest, $caminhos_possiveis);
	}

	public function tracarRotaComposta($orig, $dest, $caminhos_possiveis)
	{

		$local_comum = array();

		foreach ($caminhos_possiveis as $fullpath) {
			$linha = explode(' ', $fullpath);

			$local1  = array_search($orig, $linha);
			$local2  = array_search($dest, $linha);

			$local_comum = $this->localComumMaisProximo($local_comum, $linha, $orig, $local1);
			$local_comum = $this->localComumMaisProximo($local_comum, $linha, $dest, $local2);

		}

		if ($local_comum[$orig]['local'] == $local_comum[$dest]['local']) {
			return array(
				$orig,
				$local_comum[$orig]['local'],
				$dest,
				'km' => (float)$local_comum[$orig]['km'] + (float)$local_comum[$dest]['km']
			);
		} else {

			$origem_comum_destino = $this->procurarCaminhoDireto($local_comum[$orig]['local'], $dest);
			$destino_comum_origem = $this->procurarCaminhoDireto($local_comum[$dest]['local'], $orig);

			if ($origem_comum_destino && $destino_comum_origem) {

//				TODO: Comparar a distância entre eles

				echo "<h1> Os 2 caminhos servem!</h1>";
				echo "<h2> TODO: Qual é o melhor?</h2>";
				die;

			} elseif ($origem_comum_destino) {

				return array(
					$orig,
					$local_comum[$orig]['local'],
					$dest,
					'km' => (float)$local_comum[$orig]['km'] + (float)$origem_comum_destino[2]
				);

			} elseif ($destino_comum_origem) {

				return array(
					$orig,
					$local_comum[$dest]['local'],
					$dest,
					'km' => (float)$local_comum[$dest]['km'] + (float)$destino_comum_origem[2]
				);
			} else {
				echo "<h2> debug $orig e $dest</h2>";
				echo "<h2>CASO NÃO TRATADO</h2>";

				var_dump($local_comum);
				var_dump($origem_comum_destino);
				var_dump($destino_comum_origem);
				die;
			}
		}
	}

	public function localComumMaisProximo(array $local_comum, array $linha, $ponto, $local_encontrado)
	{
		if (is_numeric($local_encontrado)) {
			if (empty($local_comum[$ponto])) {
				if ($local_encontrado == 0) {
					$local_comum[$ponto]['local']  = $linha[1];
				} else {
					$local_comum[$ponto]['local']  = $linha[0];
				}
				$local_comum[$ponto]['km']     = (float)$linha[2];
			} else {
				if ($linha[2] < $local_comum[$ponto]['km']) {
					if ($local_encontrado == 0) {
						$local_comum[$ponto]['local']  = $linha[1];
					} else {
						$local_comum[$ponto]['local']  = $linha[0];
					}
					$local_comum[$ponto]['km']     = (float)$linha[2];
				}
			}
		}
		return $local_comum;
	}
}