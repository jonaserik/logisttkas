<!doctype html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Atualizar Mapa</title>
</head>
<body>
<form action="_result_atualizar_mapa.php" method="post">
	<table>
		<tr>
			<td class="label">Usuário</td>
			<td class="field">
				<input type="text" name="usuario" placeholder="ID - numérico inteiro. Ex: 1">
			</td>
		</tr>
		<tr>
			<td class="label">Nome do Mapa</td>
			<td class="field">
				<input type="text" name="nome">
			</td>
		</tr>
		<tr>
			<td class="label" valign="top">Malha</td>
			<td class="field">
				<textarea name="malha" id="malha" cols="30" rows="10">A B 10
B D 15
A C 20
C D 30
B E 50
D E 30</textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Enviar"></td>
		</tr>
	</table>
</form>
</body>
</html>