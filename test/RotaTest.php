<?php

require_once '../rota.php';

class RotaTest extends PHPUnit_Framework_TestCase {

	public $rota;

	public function setUp() {
		parent::setUp();
		$this->rota = new Rota();
	}

	/*
	 * @group methods
	 */
	public function testDirecaoCorreta()
	{
		$origem     = 'B';
		$destino    = 'A';
		$linha      = $this->rota->procurarCaminhoDireto($origem, $destino);
		$this->assertEquals($origem, $linha[0]);
		$this->assertEquals($destino, $linha[1]);
	}

	/*
	 * @group methods
	 */
	public function testInputRules()
	{
		$campos_origem_vazio = array(
			array (
				'campo'     => 'Origem',
				'valor'     => '',
				'tipo'      => 'string',
			),
			array (
				'campo'     => 'Destino',
				'valor'     => 'B',
				'tipo'      => 'string',
			),
			array (
				'campo'     => 'Autonomia',
				'valor'     => 11.5,
				'tipo'      => 'float',
			),
			array (
				'campo'     => 'Preco Combustivel',
				'valor'     => 3.4,
				'tipo'      => 'float',
			)
		);

		$val_campos_origem_vazio = json_encode($this->rota->inputRules($campos_origem_vazio));

		$expect01  = '["1 ERRO(S) encontrado(s)","O campo Origem precisa ser preenchido"]';
		$this->assertEquals($expect01, $val_campos_origem_vazio);

		$campos_origem_destino_vazios = array(
			array (
				'campo'     => 'Origem',
				'valor'     => '',
				'tipo'      => 'string',
			),
			array (
				'campo'     => 'Destino',
				'valor'     => '',
				'tipo'      => 'string',
			),
			array (
				'campo'     => 'Autonomia',
				'valor'     => 11.5,
				'tipo'      => 'float',
			),
			array (
				'campo'     => 'Preco Combustivel',
				'valor'     => 3.4,
				'tipo'      => 'float',
			)
		);

		$campos_origem_destino_vazios = json_encode($this->rota->inputRules($campos_origem_destino_vazios));

		$expect02  = '["2 ERRO(S) encontrado(s)","O campo Origem precisa ser preenchido",'.
		             '"O campo Destino precisa ser preenchido"]';
		$this->assertEquals($expect02, $campos_origem_destino_vazios);


		$campos_autonomia_string_vazia = array(
			array (
				'campo'     => 'Origem',
				'valor'     => 'A',
				'tipo'      => 'string',
			),
			array (
				'campo'     => 'Destino',
				'valor'     => 'B',
				'tipo'      => 'string',
			),
			array (
				'campo'     => 'Autonomia',
				'valor'     => '',
				'tipo'      => 'float',
			),
			array (
				'campo'     => 'Preco Combustivel',
				'valor'     => 3.4,
				'tipo'      => 'float',
			)
		);

		$campos_autonomia_string_vazia = json_encode($this->rota->inputRules($campos_autonomia_string_vazia));

		$expect03  = '["2 ERRO(S) encontrado(s)","O campo Autonomia precisa ser preenchido",'.
		             '"O campo Autonomia precisa ser do tipo float"]';
		$this->assertEquals($expect03, $campos_autonomia_string_vazia);

		$campos_autonomia_not_float = array(
			array (
				'campo'     => 'Origem',
				'valor'     => 'A',
				'tipo'      => 'string',
			),
			array (
				'campo'     => 'Destino',
				'valor'     => 'B',
				'tipo'      => 'string',
			),
			array (
				'campo'     => 'Autonomia',
				'valor'     => '10km',
				'tipo'      => 'float',
			),
			array (
				'campo'     => 'Preco Combustivel',
				'valor'     => 3.4,
				'tipo'      => 'float',
			)
		);

		$campos_autonomia_not_float = json_encode($this->rota->inputRules($campos_autonomia_not_float));

		$expect04  = '["1 ERRO(S) encontrado(s)","O campo Autonomia precisa ser do tipo float"]';
		$this->assertEquals($expect04, $campos_autonomia_not_float);

		$campos_combustivel_not_float = array(
			array (
				'campo'     => 'Origem',
				'valor'     => 'A',
				'tipo'      => 'string',
			),
			array (
				'campo'     => 'Destino',
				'valor'     => 'B',
				'tipo'      => 'string',
			),
			array (
				'campo'     => 'Autonomia',
				'valor'     => 11,
				'tipo'      => 'float',
			),
			array (
				'campo'     => 'Preco Combustivel',
				'valor'     => "3,65",
				'tipo'      => 'float',
			)
		);

		$campos_combustivel_not_float = json_encode($this->rota->inputRules($campos_combustivel_not_float));

		$expect04  = '["1 ERRO(S) encontrado(s)","O campo Preco Combustivel precisa ser do tipo float"]';
		$this->assertEquals($expect04, $campos_combustivel_not_float);
	}

	/*
	 * @group methods
	 */
	public function testMalhaToDB()
	{
		$malha = array(
			'A B 10',
			'B D 15',
			'A C 20',
			'C D 30',
			'B E 50',
			'D E 30'
		);
		$res = $this->rota->malhaToDB($malha);
		$expect = "A B 10;B D 15;A C 20;C D 30;B E 50;D E 30;";
		$this->assertEquals($expect, $res);
	}

	/*
	 * @group exceptions
	 */
	public function testDestinosIguais()
	{
		$dados = array(
			'orig' => 'A',
			'dest' => 'A',
			'auto' => 8,
			'fuel' => 3.3,
		);

		$rota11     = $this->rota->calculoRota($dados);
		$expect11   = '{"rota":"Origem e destino iguais","custo":0}';
		$this->assertEquals($expect11, $rota11);

	}

	/*
	 * @depends testDirecaoCorreta
	 * @group general
	 */
	public function testObterRotas()
	{
		$dados = array(
			'orig' => 'A',
			'dest' => 'B',
			'auto' => 10,
			'fuel' => 2.5,
		);
		$rota01     = $this->rota->calculoRota($dados);
		$expect01   = '{"rota":"A B","custo":2.5}';
		$this->assertEquals($expect01, $rota01);

		$dados = array(
			'orig' => 'A',
			'dest' => 'D',
			'auto' => 10,
			'fuel' => 2.5,
		);
		$rota02     = $this->rota->calculoRota($dados);
		$expect02   = '{"rota":"A B D","custo":6.25}';
		$this->assertEquals($expect02, $rota02);

		$dados = array(
			'orig' => 'A',
			'dest' => 'B',
			'auto' => 8,
			'fuel' => 3.3,
		);
		$rota03     = $this->rota->calculoRota($dados);
		$expect03   = '{"rota":"A B","custo":4.125}';
		$this->assertEquals($expect03, $rota03);

		$dados = array(
			'orig' => 'A',
			'dest' => 'E',
			'auto' => 10,
			'fuel' => 2.5,
		);
		$rota04     = $this->rota->calculoRota($dados);
		$expect04   = '{"rota":"A B E","custo":15}';
		$this->assertEquals($expect04, $rota04);

		$dados = array(
			'orig' => 'B',
			'dest' => 'C',
			'auto' => 10,
			'fuel' => 2.5,
		);
		$rota05     = $this->rota->calculoRota($dados);
		$expect05   = '{"rota":"B A C","custo":7.5}';
		$this->assertEquals($expect05, $rota05);

		$dados = array(
			'orig' => 'E',
			'dest' => 'C',
			'auto' => 10,
			'fuel' => 2.5,
		);
		$rota06     = $this->rota->calculoRota($dados);
		$expect06   = '{"rota":"E D C","custo":15}';
		$this->assertEquals($expect06, $rota06);

		$dados = array(
			'orig' => 'E',
			'dest' => 'A',
			'auto' => 10,
			'fuel' => 2.5,
		);
		$rota07     = $this->rota->calculoRota($dados);
		$expect07   = '{"rota":"E B A","custo":15}';
		$this->assertEquals($expect07, $rota07);

		$dados = array(
			'orig' => 'D',
			'dest' => 'E',
			'auto' => 10,
			'fuel' => 2.5,
		);
		$rota08     = $this->rota->calculoRota($dados);
		$expect08   = '{"rota":"D E","custo":7.5}';
		$this->assertEquals($expect08, $rota08);

		$dados = array(
			'orig' => 'E',
			'dest' => 'D',
			'auto' => 10,
			'fuel' => 2.5,
		);
		$rota09     = $this->rota->calculoRota($dados);
		$expect09   = '{"rota":"E D","custo":7.5}';
		$this->assertEquals($expect09, $rota09);
	}

}