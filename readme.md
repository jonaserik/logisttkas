# Logisttkas

Webservice de cálculo de rotas escrito em PHP orientado a objeto com retornos em JSON.

## Requisitos
* PHP 5.5
* Apache 2.4.9
* PHPUnit 4.8
* Composer

> Caso haja alguma dificuldade no setup, por favor, me avise.

> Um dump com a estrutura do bando está na raiz. Aquivo: logisttkas_dump.sql

Ele se utiliza de cargas de malha logística e conta com dois métodos principais: obterRota() e atualizarMapa().

## Método obterRota()

Basicamente recebe via POST os dados de **origem** e **destino** desejados e traça a melhor rota entre os 2 locais.

## Parâmetros
O **obterRota()** deve receber como parâmetros os seguintes dados:
* Origem - String
* Destino - String
* Autonomia - Float
* Preço do combustível - Float

## Exemplo

### Requisição
Conteúdo do $_POST:
``` php
array (size=4)
  'origem' => string 'A' (length=1)
  'destino' => string 'B' (length=1)
  'autonomia' => string '10' (length=2)
  'preco_combustivel' => string '2.5' (length=3)
```

### Resposta
``` JSON
{"rota":"A B","custo":2.5}
```

## Método atualizarMapa()

Dá a possibilidade do usuário carregar um novo mapa.

## Parâmetros
* Usuário - Integer
* Nome [do mapa]- String
* Malha - formato Malha Logistica

## Exemplo

### Requisição

Conteúdo do $_POST:
``` php
array (size=3)
  'usuario' => string '1' (length=1)
  'nome' => string 'Rio' (length=3)
  'malha' => string 'A B 10
B D 15
A C 20
C D 30
B E 50
D E 30' (length=46)
```

### Resposta

Confirmação
``` JSON
["OK","Mapa atualizado com sucesso"]
```

## Exemplos de uso

O arquivo *index.php* contém links para formulários realizam requisições para os serviços.


## Bibliotecas

As bibliotecas de terceiros só foram usadas para queries de banco de dados e testes unitários. São elas:
* PHPUnit na versão 4.8
* MysqliDb de Joshcam
``` JSON
{
    "require": {
        "phpunit/phpunit": "4.8.*",
        "joshcam/mysqli-database-class": "dev-master"
    }
}

```

Como elas não têm participação na lógica de roteamento e nem nos tratamentos do webservice, acredito que
elas não infrinjam as regras do teste.

## Processo de desenvolvimento

Em primeiro momento, foquei mais na lógica das rotas. Mais do que em qualquer coisa relacionada à implementação;
Depois me voltei aos testes unitários e refatoração da classe de roteamento; E por último, foquei nas requisições
e consumo do serviço via POST.


## A partir daqui

Ainda estou refatorando a implementação. E continuarei subindo fixes e mais testes unitários. Curti do teste
 sua proposta de utilidade e complexidade me lembraram a época da faculdade.
