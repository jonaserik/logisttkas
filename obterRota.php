<!doctype html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Obter Rota</title>
</head>
<body>
<form action="_result_obter_rota.php" method="post">
	<table>
		<tr>
			<td class="label">Origem</td>
			<td class="field">
				<input type="text" name="origem">
			</td>
		</tr>
		<tr>
			<td class="label">Destino</td>
			<td class="field">
				<input type="text" name="destino">
			</td>
		</tr>
		<tr>
			<td class="label">Autonomia</td>
			<td class="field">
				<input type="text" name="autonomia">
			</td>
		</tr>
		<tr>
			<td class="label">Preço Combustível</td>
			<td class="field">
				<input type="text" name="preco_combustivel">
			</td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Enviar"></td>
		</tr>
	</table>
</form>
</body>
</html>