<?php

require_once 'vendor/joshcam/mysqli-database-class/MysqliDb.php';

class Query
{
	protected static $instance = null;

	public static function get_instance(){
		if(self::$instance === null)
		{
			self::$instance = new self();
		}
		return self::$instance;
	}

	public static function getMapaAtual($user)
	{
		$db = MysqliDb::getInstance();
		return $db->rawQuery("SELECT * FROM mapa WHERE usuario_id = $user ORDER BY id DESC LIMIT 1;");
	}

	public static function setNovoMapa($user, $nome, $malha)
	{
		$db = MysqliDb::getInstance();

		$dados = array(
			'usuario_id'    => $user,
			'nome'          => $nome,
			'malha'         => $malha,
		);

		$id = $db->insert ('mapa', $dados);
		if ($id) {
			return true;
		} else {
			return false;
		}
	}

	public static function authUsuario($user, $token)
	{
		$db = MysqliDb::getInstance();
		$res = $db->rawQuery("SELECT * FROM usuario WHERE id = $user AND token = '$token';");
		if (empty($res)) {
			return false;
		}
		return true;
	}

}

if ( ! is_object(MysqliDb::getInstance())) {
	$config['database']   = array(
		'host'      => 'localhost',
		'username'  => 'root',
		'password'  => '',
		'db'        => 'logisttikas',
		'port'      => 3306,
		'charset'   => 'utf8'
	);
	$db = new MysqliDb ($config['database']);
}
